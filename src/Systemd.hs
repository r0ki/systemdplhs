{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE OverloadedStrings #-}

module Systemd (
    listUnits,
    listUnitsFiltered,
    listUnitsByPatterns,
    listUnitsByNames,
    startUnit,
    stopUnit,
    reloadUnit,
    restartUnit,
    tryRestartUnit,
    killUnit
) where

import DBus
import DBus.Client
import Data.Int (Int32)

type Unit = String
type Mode = String
type SignalNum = Int32

systemdObjectPath :: ObjectPath
systemdObjectPath = objectPath_ "/org/freedesktop/systemd1"

systemdInterfaceName :: InterfaceName
systemdInterfaceName = interfaceName_ "org.freedesktop.systemd1.Manager"

systemdDestination :: BusName
systemdDestination = busName_ "org.freedesktop.systemd1"

methodSub :: String -> MethodCall
methodSub = methodCall systemdObjectPath systemdInterfaceName . memberName_

systemdCall :: Client -> MethodCall -> IO [Variant]
systemdCall = (.) (fmap methodReturnBody) . call_

listUnits :: Client -> IO [Variant]
listUnits = flip systemdCall (methodSub "ListUnits") {
    methodCallDestination = Just systemdDestination
}

listUnitsFiltered :: IsValue a => Client -> [a] -> IO [Variant]
listUnitsFiltered cli var = systemdCall cli (methodSub "ListUnitsFiltered") {
    methodCallDestination = Just systemdDestination,
    methodCallBody = [toVariant var]
}

listUnitsByPatterns :: IsValue a => Client -> [a] -> [a] -> IO [Variant]
listUnitsByPatterns cli states patterns = systemdCall cli (methodSub "ListUnitsByPatterns") {
    methodCallDestination = Just systemdDestination,
    methodCallBody = map toVariant [states, patterns]
}

listUnitsByNames :: IsValue a => Client -> [a] -> IO [Variant]
listUnitsByNames cli var = systemdCall cli (methodSub "ListUnitsByNames") {
    methodCallDestination = Just systemdDestination,
    methodCallBody = [toVariant var]
}

controlUnit :: String -> Client -> Unit -> Mode -> IO [Variant]
controlUnit med cli unit mode = systemdCall cli (methodSub med) {
    methodCallDestination = Just systemdDestination,
    methodCallBody = map toVariant [unit, mode]
}

startUnit :: Client -> Unit -> Mode -> IO [Variant]
startUnit = controlUnit "StartUnit"

stopUnit :: Client -> Unit -> Mode -> IO [Variant]
stopUnit = controlUnit "StopUnit"

reloadUnit :: Client -> Unit -> Mode -> IO [Variant]
reloadUnit = controlUnit "ReloadUnit"

restartUnit :: Client -> Unit -> Mode -> IO [Variant]
restartUnit = controlUnit "RestartUnit"

tryRestartUnit :: Client -> Unit -> Mode -> IO [Variant]
tryRestartUnit = controlUnit "TryRestartUnit"

killUnit :: Client -> Unit -> Mode -> SignalNum -> IO [Variant]
killUnit cli unit mode sig = systemdCall cli (methodSub "KillUnit") {
    methodCallDestination = Just systemdDestination,
    methodCallBody = (map toVariant [unit, mode]) ++ [toVariant sig]
}
