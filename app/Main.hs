import DBus.Client (connectSystem)
import System.Environment (getArgs)
import Control.Monad (mapM_, (<=<))
import Systemd

main :: IO ()
main = do
    client <- connectSystem
    args <- getArgs
    mapM_ (print <=< flip (startUnit client) "replace") args
    print =<< listUnitsByNames client args
    mapM_ (print <=< flip (stopUnit client) "replace") args
    print =<< listUnitsByNames client args
    print =<< listUnitsByPatterns client ["loaded"] args

